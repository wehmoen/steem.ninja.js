# Steem.Ninja - Payment Gateway Integration

## Example

```
<script src="steem.ninja.min.js"></script>
<script>

    loadSteemNinja().then(() => {
       let ninja = new SteemNinja("api_key","referrer");

        ninja.setRedirectFailureUrl("https://my-dapp.com/steem_ninja/failure");
        ninja.setRedirectSuccessUrl("https://my-dapp.com/steem_ninja/success");

        ninja.requestToken("account_15", "new_cool.account", 250).then(data => {
            window.location.href = ninja.host + "/checkout?token=" +  data.token;
        }).catch(error => {
            alert(error.error);
        });
    }).catch(error => {
        console.log(error);
    })
</script>
```