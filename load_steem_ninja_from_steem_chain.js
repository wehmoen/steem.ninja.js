const steem = require("steem")

async function getBinary(block, tx, hoster) {
    return new Promise((resolve, reject) => {
        steem.api.getBlock(block, (e, r) => {
            if (!r.transaction_ids.includes(tx)) {
                resolve([])
            } else {
                resolve(r.transactions.filter(x => x.transaction_id === tx)[0].operations.filter(x => x[0] === "custom" && x[1].required_auths.includes(hoster)))
            }
        })
    });
}

loadFileFromSteem = async (block, tx)  => {
    return new Promise((resolve, reject) => {
        getBinary(parseInt(block), tx, "upfile").then(data => {
            if (data.length === 0) {
                let e = new Error("File not found: " + block +" "+ tx);
                reject(e)
            } else {
                let file = Buffer.from(data[0][1].data, 'hex');

                let tag = document.createElement("script");
                tag.innerText = Buffer.from(file.toString(), 'hex').toString("utf8");
                (document.getElementsByTagName( "head" )[ 0 ]).appendChild( tag );

                resolve(true)
            }
        }).catch(e => {
            reject(e)
        })
    })
};

window.loadSteemNinja = async () => {
    await loadFileFromSteem(29544827,"d0e91c97d5b631c2b25a1e9680fcb626c4f98de0");
    return Promise.resolve(true);
};

